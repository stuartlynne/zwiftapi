#
# Quick and Simple perl module to access Zwift secure API
#
# Copyright (c) 2018 Stuart.Lynne@gmail.com
#
# Distributed with BSD License, you may reuse with attribution
#

package ZwiftAPI;

use strict;
use warnings;

use LWP::UserAgent ();
use JSON::Tiny qw(decode_json);
use Data::Dumper;

#use Lib::OOP qw(Configure GetSet Unknown FUNC);

use Carp;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @EXPORT_TAGS) ;

$VERSION        = 1.00;
@ISA            = qw(Exporter);
@EXPORT         = qw(get_json get_protobuf );

### constructor
# _init
#
sub _init {
    my $self = shift;
}

# configure
#
sub Unknown {
    my $class = shift;
    my %params = @_;
    croak "Unrecognised configuration keys for $class - " . join( " ", keys %params );
}

sub doConfigure {

    my ($class, $keys, $params) = @_;
    my @keys = @{$keys} ;
    my %params = %{$params};

    foreach (@{$keys}) {
        $class->{"$_"} = delete $params{$_} if exists $params{$_};
    }
    Unknown($class, %params) if keys %params;
}

sub configure {
    my $self = shift;
    my %params = @_;
    doConfigure($self, [qw(username password)], \%params);
}


###
# new
#
sub new {

    my $class = shift;
    my %params = @_;

    return undef unless %params;
    return undef unless(defined $params{username});
    return undef unless(defined $params{password});

    # the internal structure we'll use to represent
    my $self = bless {}, $class;

    $self->configure( %params );

    my $access_token = get_access_token($params{username}, $params{password});

    return undef unless ($access_token);

    $self->{base_url} = 'https://us-or-rly101.zwift.com';
    $self->{access_token} = $access_token;

    return $self;
}

# close
sub close {
    my $self = shift;
}

### accessors
#sub access_token {return OOP::GetSet(shift @_, 'access_token', @_);}
#sub base_url {return OOP::GetSet(shift @_, 'base_url', @_);}


### methods

# ZwiftAPI::get_json
# Get json response from zwift for url, access_token previously retrieved.
#
sub ZwiftAPI::get_json {
    my $self = shift;
    my ($url) = @_;

    my $ua = LWP::UserAgent->new();
    my $response = $ua->get( $self->{base_url} . $url,
            #'Content-Type' => 'application/json; charset=UTF-8', 
            Accept => 'application/json',
            Authorization => 'Bearer '. $self->{access_token});

    return undef unless (check_response($response));
    return $response->decoded_content();
}

# ZwiftAPI::get_protobuf
# Get protobut response from zwift for url, access_token previously retrieved.
#
sub ZwiftAPI::get_protobuf {
    my $self = shift;
    my ($url) = @_;

    my $ua = LWP::UserAgent->new();
    my $response = $ua->get( $self->{base_url} . $url,
            #'Content-Type' => 'application/json; charset=UTF-8', 
            Accept => 'application/x-protobuf-lite',
            Authorization => 'Bearer '. $self->{access_token});

    return undef unless (check_response($response));
    return $response->decoded_content();
}


# internal

# check for error 
#
sub check_response {
    my ($response) = @_;

    return 1 if ($response->is_success());
    printf STDERR "Failed: Status:%s\n", $response->status_line();
    printf STDERR "Failed: %s\n", Dumper($response);
    return undef;
}

# get access token from zwift
#
sub get_access_token {
    my ($username, $password) = @_;

    my %Form = (
            client_id => "Zwift_Mobile_Link",
            username => $username,
            password => $password,
            grant_type => "password"
            );

    my $ua = LWP::UserAgent->new();
    my $response = $ua->post(
            "https://secure.zwift.com/auth/realms/zwift/tokens/access/codes",
            \%Form,
            );

    return undef unless (check_response($response));

    my $decoded_content = decode_json($response->decoded_content());

    return $decoded_content->{access_token};
}


