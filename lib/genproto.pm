#!/usr/bin/perl


use Google::ProtocolBuffers;

 ## in helper script
#Google::ProtocolBuffers->parse(
#        "message Foo {optional int32 a = 1; }",
#        { generate_code => 'Foo.pm' }
#        );
#Google::ProtocolBuffers->parse(
#        "message Foo {optional int32 a = 1; }", 
#        { generate_code => "test.pm"}
#   );



Google::ProtocolBuffers->parsefile(
        $ARGV[0],
        {
            simple_extensionis => 1, 
            create_accessors => 1,
            generate_code => $ARGV[1],
        }

    );
exit();
